import 'package:flutter/material.dart';

class PageTwo extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  String pict = "assets/img/profile.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 8),
                child: CircleAvatar(
                  maxRadius: 80,
                  child: ClipOval(
                    child: Image(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        pict,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16, bottom: 16),
                child: Text("Nopal", style: TextStyle(fontSize: 30),),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Icon(Icons.calendar_today),
                        Text("Tanggal Lahir")
                      ],
                    ),
                  ),
                  Expanded(child:Column(
                    children: <Widget>[
                      Icon(Icons.favorite),
                      Text("Hobby")
                    ],
                  ),),
                  
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Icon(Icons.fastfood),
                        Text("Makanan favorit")
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
